package orel.dmitriy.util;

@FunctionalInterface
public interface CallBack<P, R> {
    void callBack(P param, R response);
}
