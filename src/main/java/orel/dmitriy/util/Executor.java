package orel.dmitriy.util;

@FunctionalInterface
public interface Executor<P, R> {
    R execute(P parameter);
}
