package orel.dmitriy.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JsonUtil {

    private static final Logger logger = LoggerFactory.getLogger(JsonUtil.class);

    public String getValue(JsonNode node, String... steps){
        if (node == null){
            return "";
        }
        JsonNode currentNode = node;
        for(String step: steps){
            currentNode = currentNode.get(step);
            if(currentNode == null){
                return "";
            }
        }
        String body = currentNode.asText();
        logger.debug("steps: {} result: {} node: {}", steps, body.replaceAll("\\n", "\\\\n"),  node.toString().replaceAll("\\n", "\\\\n"));
        return body;
    }

    public JsonNode getJsonNode(String jsonData) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readTree(jsonData);
    }
}
