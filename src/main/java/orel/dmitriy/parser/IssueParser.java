package orel.dmitriy.parser;

import com.fasterxml.jackson.databind.JsonNode;
import orel.dmitriy.model.Issue;
import orel.dmitriy.util.JsonUtil;
import orel.dmitriy.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Component
public class IssueParser {

    private static final Logger logger = LoggerFactory.getLogger(IssueParser.class);

    @Autowired
    private JsonUtil jsonUtil;

    public List<Issue> parse(String json) {
        List<Issue> issues = new LinkedList<>();
        try {
            JsonNode objects = jsonUtil.getJsonNode(json);
            for (Iterator<JsonNode> it = objects.elements(); it.hasNext(); ) {
                JsonNode element = it.next();
                try {
                    Issue issue = new Issue();
                    issue.setBody(jsonUtil.getValue(element, "body"));
                    issue.setTitle(jsonUtil.getValue(element, "title"));
                    issue.setUrl(jsonUtil.getValue(element, "url"));
                    if (StringUtils.isNotEmpty(issue.getUrl())){
                        issues.add(issue);
                    } else {
                        logger.debug("Can't convert to Issue from element: {}", element);
                    }
                } catch (Exception e) {
                    logger.debug("Can't convert to Issue from element: {}", element);
                }
            }
        } catch (Exception e) {
            logger.error("Can't convert to Issues from json: {}", json);
        }
        return issues;
    }

}
