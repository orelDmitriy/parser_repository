package orel.dmitriy.parser;

import com.fasterxml.jackson.databind.JsonNode;
import orel.dmitriy.model.Npm;
import orel.dmitriy.util.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Component
public class NpmParser {

    private static final Logger logger = LoggerFactory.getLogger(NpmParser.class);

    @Autowired
    private JsonUtil jsonUtil;

    public List<Npm> parse(String jsonData) {
        List<Npm> listDto = new LinkedList<>();
        try {
            JsonNode objects = jsonUtil.getJsonNode(jsonData);
            for (Iterator<JsonNode> it = objects.elements(); it.hasNext(); ) {
                JsonNode element = it.next();
                try {
                    Npm npm = new Npm();
                    npm.setName(element.get("name").asText());
                    npm.setVersion(jsonUtil.getValue(element, "dist-tags", "latest"));
                    npm.setModified(jsonUtil.getValue(element, "time", "modified"));
                    npm.setRepositoryUrl(jsonUtil.getValue(element, "repository", "url"));
                    npm.setRepositoryType(jsonUtil.getValue(element, "repository", "type"));
                    npm.setBugsUrl(jsonUtil.getValue(element, "bugs", "url"));
                    npm.setLicense(jsonUtil.getValue(element, "license"));
                    listDto.add(npm);
                } catch (Exception e) {
                    logger.error("Can't convert to Npm from element: {}", element);
                }
            }
        } catch (Exception e) {
            logger.error("Can't convert to Issues from json: {}", jsonData);
        }
        return listDto;
    }
}
