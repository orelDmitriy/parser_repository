package orel.dmitriy.parser;

import com.fasterxml.jackson.databind.JsonNode;
import orel.dmitriy.model.Git;
import orel.dmitriy.util.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GitParser {

    private static final Logger logger = LoggerFactory.getLogger(GitParser.class);

    @Autowired
    private JsonUtil jsonUtil;

    public Git parse(String jsonData) {
        Git git = new Git();
        try {
            JsonNode element = jsonUtil.getJsonNode(jsonData).get("items").get(0);
            logger.debug("got element: {}", element.toString());
            git.setFullName(jsonUtil.getValue(element, "full_name"));
            git.setIssuesUri(jsonUtil.getValue(element, "issues_url"));
            git.setOpenIssues(Integer.parseInt(jsonUtil.getValue(element, "open_issues")));
        } catch (Exception e) {
            logger.error("Can't convert to Git from json: {}", jsonData);
        }
        return git;
    }

}
