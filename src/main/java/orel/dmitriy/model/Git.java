package orel.dmitriy.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@ToString
@Entity
@Table(name = "git")
public class Git {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique=true, nullable = false)
    private String fullName;

    @Column
    private String issuesUri;

    @Column
    private int openIssues;

    @OneToOne(fetch=FetchType.LAZY, mappedBy="git")
    private Npm npm;

    @OneToMany(mappedBy="git")
    private List<Issue> issues;

}
