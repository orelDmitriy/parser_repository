package orel.dmitriy.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@Table(name = "issue")
public class Issue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true, nullable = false)
    @Type(type="text")
    private String url;

    @Column
    @Type(type="text")
    private String title;

    @Column
    @Type(type="text")
    private String body;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="issues")
    private Git git;
}
