package orel.dmitriy.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ResultDto {
    private String uri;
    private String data;

    public ResultDto(String uri, String data) {
        this.uri = uri;
        this.data = data;
    }

}
