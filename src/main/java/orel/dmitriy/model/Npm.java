package orel.dmitriy.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@Table(name = "npm")
public class Npm {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    @Type(type="text")
    private String repositoryUrl;
    @Column
    private String repositoryType;
    @Column
    @Type(type="text")
    private String bugsUrl;
    @Column
    @Type(type="text")
    private String license;
    @Column
    private String modified;
    @Column
    private String version;
    @Column(nullable = false, unique = true)
    private String name;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private Git git;

}
