package orel.dmitriy.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(exclude={"whenUnblocked", "unlock", "SECOND"})
@ToString(exclude={"whenUnblocked", "unlock", "SECOND"})
public class Proxy {
    private final long SECOND = 1000000000l;

    @Getter
    private String proxyIp;
    @Getter
    private int proxyPort;
    private long whenUnblocked;
    @Setter
    private boolean unlock;

    public Proxy(String proxyIp, int proxyPort) {
        this.proxyIp = proxyIp;
        this.proxyPort = proxyPort;
        whenUnblocked = 0;
        unlock = true;
    }

    public void setWhenUnblocked() {
        this.whenUnblocked = System.nanoTime() + 50 * SECOND;
    }

    public boolean isUnlocked(){
        long time = System.nanoTime();
        return whenUnblocked < time && unlock;
    }

}
