package orel.dmitriy.service;

import orel.dmitriy.util.CallBack;
import orel.dmitriy.util.Executor;

public interface ThreadService {
    void waitUntilAllThreads();
    <P, R> void runNewThread(Executor<P, R> func, P parameter, CallBack<P, R> callBack);
}
