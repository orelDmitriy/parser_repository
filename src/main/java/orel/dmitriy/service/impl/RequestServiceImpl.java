package orel.dmitriy.service.impl;

import orel.dmitriy.Settings;
import orel.dmitriy.model.Proxy;
import orel.dmitriy.service.FileService;
import orel.dmitriy.service.ProxyPullService;
import orel.dmitriy.service.RequestService;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Random;

@Service
@Scope("singleton")
public class RequestServiceImpl implements RequestService {

    private static final Logger logger = LoggerFactory.getLogger(RequestServiceImpl.class);

    @Autowired
    private ProxyPullService proxyPullService;

    @Autowired
    private FileService fileService;

    @PostConstruct
    public void init(){
        List<String> lines = fileService.getLines(Settings.PROXY_FILE_PATH);
        String[] proxyData;
        Proxy proxy;
        for(String line: lines){
            try {
                proxyData = line.split(";");
                logger.debug("new proxy: {}:{}", proxyData[0], proxyData[1]);
                proxy = new Proxy(proxyData[0], Integer.parseInt(proxyData[1]));
                proxyPullService.addProxy(proxy);
            } catch (Exception e) {
                logger.error("line: {}", line, e);
            }
        }
        logger.info("load {} proxy", proxyPullService.size());
    }

    @Override
    public String getJson(String uri) {
        String json = getJson(uri, proxyPullService.getUnlockProxy());
        logger.debug("for uri: {} response size: {}", uri, json.length());
        return json;
    }

    @Override
    public String getBigJson(String uri){
        try {
            String result = "";
            HttpsURLConnection con = (HttpsURLConnection) new URL(uri).openConnection();
            BufferedReader br =
                    new BufferedReader(
                            new InputStreamReader(con.getInputStream()));
            String input;
            while ((input = br.readLine()) != null){
                result += input;
            }
            br.close();
            return result;
        } catch (Exception e) {
            logger.error("", e);
        }
        return "";
    }

    @Override
    public String getJsonFromFile(String filePath) {
        List<String> lines = fileService.getLines(filePath);
        String result = "";
        for(String line: lines){
            result += line;
        }
        return result;
    }

    private String getJson(String uriStr, Proxy currentProxy) {
        try {
            Random random = new Random();
            int timeOut = Settings.TIME_OUT_MINIMUM + random.nextInt(Settings.TIME_OUT_DELTA);
            Connection.Response response = Jsoup.connect(uriStr)
                    .proxy(currentProxy.getProxyIp(), currentProxy.getProxyPort())
                    .ignoreContentType(true)
                    .method(Connection.Method.GET)
                    .userAgent(Settings.USER_AGENT)
                    .timeout(timeOut * Settings.SECOND)
                    .execute();
            String result = response.body();
            currentProxy.setUnlock(true);
            return result;
        } catch (Exception e) {
            logger.info("Proxy was blocked: {}, for uri: {}", currentProxy, uriStr);
            blockCurrentProxy(currentProxy);
            Proxy proxy = proxyPullService.getUnlockProxy();
            return getJson(uriStr, proxy);
        }
    }

    private void blockCurrentProxy(Proxy currentProxy) {
        currentProxy.setWhenUnblocked();
        currentProxy.setUnlock(true);
    }

}
