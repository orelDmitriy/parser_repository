package orel.dmitriy.service.impl;

import orel.dmitriy.model.Npm;
import orel.dmitriy.service.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

@Service
public class FileServiceImpl implements FileService {

    private static final Logger logger = LoggerFactory.getLogger(RequestServiceImpl.class);

    @Override
    public void createOrClearFile(String path){
        try {
            File file = new File(path);
            if(file.exists()){
                file.delete();
            }
            file.createNewFile();
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    @Override
    public void append(String path, String text){
        try {
            FileWriter writer = new FileWriter(path, true);
            BufferedWriter bufferWriter = new BufferedWriter(writer);
            bufferWriter.write(text);
            bufferWriter.flush();
            bufferWriter.close();
            writer.close();
        }
        catch (IOException e) {
            logger.error("", e);
        }
    }

    @Override
    public List<String> getLines(String path) {
        List<String> result = new LinkedList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line;
            while((line = br.readLine()) != null){
                result.add(line);
            }
            br.close();
        }  catch (IOException e) {
            logger.error("", e);
        }
        return result;
    }

    public void writeFile(List<Npm> listDto) {
        String path = "D:\\npm1.txt";
        createOrClearFile(path);
        listDto.stream().forEach(dto -> {
            append(path,dto.toString());
        });
    }

    public void writeFile(String string) {
        String path = "D:\\npmString3.txt";
        createOrClearFile(path);
        append(path,string);
    }
}
