package orel.dmitriy.service.impl;

import orel.dmitriy.Settings;
import orel.dmitriy.service.ThreadService;
import orel.dmitriy.util.CallBack;
import orel.dmitriy.util.Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@Scope("prototype")
public class ThreadServiceImpl implements ThreadService {

    private static final Logger logger = LoggerFactory.getLogger(ThreadServiceImpl.class);

    private List<Long> threads;

    public ThreadServiceImpl(){
        threads = new LinkedList<>();
    }

    @Override
    public void waitUntilAllThreads() {
        while (!threads.isEmpty()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public synchronized <P, R> void runNewThread(Executor<P, R> func, P parameter, CallBack<P, R> callBack) {
        waitFreeThread();
        Thread thread = new Thread(()->{
            logger.debug("{} started", Thread.currentThread().getName());
            R response = func.execute(parameter);
            callBack.callBack(parameter, response);
            threads.remove(Thread.currentThread().getId());
        });
        threads.add(thread.getId());
        thread.start();
    }

    private synchronized void waitFreeThread() {
        logger.debug("{} wait", Thread.currentThread().getName());
        while (threads.size() >= Settings.MAX_THREADS){
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                logger.error("", e);
            }
        }
    }
}
