package orel.dmitriy.service.impl;

import orel.dmitriy.model.Git;
import orel.dmitriy.model.Issue;
import orel.dmitriy.parser.IssueParser;
import orel.dmitriy.repository.IssueRepository;
import orel.dmitriy.service.IssueService;
import orel.dmitriy.service.RequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@Scope("prototype")
public class IssueServiceImpl implements IssueService {

    private static final Logger logger = LoggerFactory.getLogger(IssueServiceImpl.class);

    @Autowired
    private RequestService requestService;

    @Autowired
    private IssueParser issueParser;

    @Autowired
    private IssueRepository issueRepository;

    private List<Issue> result;

    @Override
    public List<Issue> getAndSaveInfoFromGitUrls(Git git) {
        result = new LinkedList<>();
        if (git.getOpenIssues() > 0) {
            logger.debug("{}", git.getIssuesUri());
            git.setIssuesUri(git.getIssuesUri().replace("{/number}", ""));
            String json = requestService.getJson(git.getIssuesUri());
            List<Issue> issues = issueParser.parse(json);
            logger.debug("git {} has {} issues", git.getFullName(), issues.size());
            for(Issue issue: issues) {
                logger.error("save issue: {}", issue);
                issueRepository.save(issue);
            }
            result.addAll(issues);
        }
        return result;
    }


}
