package orel.dmitriy.service.impl;

import orel.dmitriy.model.ResultDto;
import orel.dmitriy.service.RequestService;
import orel.dmitriy.service.SenderService;
import orel.dmitriy.service.ThreadService;
import orel.dmitriy.util.CallBack;
import orel.dmitriy.util.Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class SenderServiceImpl implements SenderService {

    private static final Logger logger = LoggerFactory.getLogger(SenderServiceImpl.class);
    private static int count = 0;

    @Autowired
    private RequestService requestService;

    @Autowired
    private ThreadService threadService;

    private List<ResultDto> result;


    public List<ResultDto> sendAll(List<String> urls) {
        result = new LinkedList<>();
        sendRequestInIndividualThreads(urls);
        threadService.waitUntilAllThreads();
        return result;
    }

    private void sendRequestInIndividualThreads(List<String> urls) {
        while (!urls.isEmpty()) {
            String uri = urls.remove(0);
            Executor<String, String> executor = requestService::getJson;
            CallBack<String, String> callBack = this::save;
            threadService.runNewThread(executor, uri, callBack);
        }
    }



    private synchronized void save(String uri, String json) {
        count++;
        logger.debug("{}: save result for uri: {}",count, uri);
        result.add(new ResultDto(uri, json));
    }

}
