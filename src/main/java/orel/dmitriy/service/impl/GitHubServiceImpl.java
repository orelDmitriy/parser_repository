package orel.dmitriy.service.impl;

import orel.dmitriy.model.Git;
import orel.dmitriy.model.Npm;
import orel.dmitriy.parser.GitParser;
import orel.dmitriy.repository.GitRepository;
import orel.dmitriy.service.GitHubService;
import orel.dmitriy.service.IssueService;
import orel.dmitriy.service.RequestService;
import orel.dmitriy.service.ThreadService;
import orel.dmitriy.util.CallBack;
import orel.dmitriy.util.Executor;
import orel.dmitriy.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Scope("prototype")
public class GitHubServiceImpl implements GitHubService {

    private static final Logger logger = LoggerFactory.getLogger(GitHubServiceImpl.class);

    @Autowired
    private RequestService requestService;

    @Autowired
    private ThreadService threadService;

    @Autowired
    private GitParser gitParser;

    @Autowired
    private IssueService issueService;

    @Autowired
    private GitRepository gitRepository;

    private final Pattern pattern = Pattern.compile("(?<=github\\.com)((/[-_\\d\\w]+){2})");

    private List<Git> result;
    private List<Npm> badUrl;

    @Override
    @Transactional
    public List<Git> getAndSaveInfoFromNpmUrls(List<Npm> correctNpm) {
        logger.debug("Count npm: {}", correctNpm.size());
        result = new LinkedList<>();
        badUrl = new LinkedList<>();
        for (Npm npm : correctNpm) {
            Executor<Npm, Git> executor = this::execute;
            CallBack<Npm, Git> callBack = this::save;
            threadService.runNewThread(executor, npm, callBack);
        }
        threadService.waitUntilAllThreads();
        return result;
    }

    @Transactional
    private Git execute(Npm npm) {
        String uri = getCorrectUri(npm);
        String json = requestService.getJson(uri);
        Git git = gitParser.parse(json);
        git.setNpm(npm);
        return git;
    }

    private synchronized void save(Npm npm, Git git) {
        if (StringUtils.isNotEmpty(git.getFullName())) {
            logger.debug("uri: {} git: {}", npm.getRepositoryUrl(), git);
            gitRepository.save(git);
            result.add(git);
            //TODO при сохранения issues в базу, некоторые issue не сохраняет из-за некоректных символов в полях title или body
            /*List<Issue> issues = issueService.getAndSaveInfoFromGitUrls(git);
            git.setIssues(issues);*/
        } else {
            logger.debug("Npm has bad git url: {}", npm.getRepositoryUrl());
            badUrl.add(npm);
        }
    }

    private String getCorrectUri(Npm npm) {
        String repositoryUrl = npm.getRepositoryUrl();
        Matcher matcher = pattern.matcher(repositoryUrl);
        if (matcher.find()) {
            String correctUrl = "https://api.github.com/search/repositories?q=" + repositoryUrl.substring(matcher.start() + 1, matcher.end()) + " in:full_name&per_page=1";
            logger.debug("for uri: {} got: {}", npm.getRepositoryUrl(), correctUrl);
            return correctUrl;
        }
        logger.error("Не найдет паттерн: {} в {}", pattern.toString(), npm.getRepositoryUrl());
        return "";
    }

    public List<Npm> getBadUrl() {
        return badUrl;
    }
}
