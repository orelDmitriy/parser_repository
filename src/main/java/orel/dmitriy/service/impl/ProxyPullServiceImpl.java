package orel.dmitriy.service.impl;

import orel.dmitriy.model.Proxy;
import orel.dmitriy.service.ProxyPullService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Scope("singleton")
class ProxyPullServiceImpl implements ProxyPullService {

    private static final Logger logger = LoggerFactory.getLogger(ProxyPullServiceImpl.class);

    private final int SECOND = 1000;

    private Set<Proxy> proxyList;

    public ProxyPullServiceImpl() {
        proxyList = new HashSet<>();
    }

    @Override
    public void addProxy(Proxy proxy) {
        proxyList.add(proxy);
    }

    @Override
    public void addProxy(String ip, int port) {
        addProxy(new Proxy(ip, port));
    }

    @Override
    public synchronized Proxy getUnlockProxy() {
        for (Proxy proxy : proxyList) {
            if (proxy.isUnlocked()) {
                proxy.setUnlock(false);
                return proxy;
            }
        }
        try {
            logger.debug("All proxy is locked, wait 1 second");
            Thread.sleep(SECOND);
        } catch (InterruptedException e) {
            logger.error("Sleep error", e);
        }
        return getUnlockProxy();
    }

    @Override
    public int size() {
        return proxyList.size();
    }
}
