package orel.dmitriy.service.impl;

import orel.dmitriy.model.Npm;
import orel.dmitriy.repository.NpmRepository;
import orel.dmitriy.service.NpmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class NpmServiceImpl implements NpmService {

    private static final Logger logger = LoggerFactory.getLogger(NpmServiceImpl.class);

    @Autowired
    private NpmRepository npmRepository;

    @Override
    public void save(List<Npm> repositories) {
        for(Npm repository: repositories){
            try {
                repository.setId(npmRepository.save(repository).getId());
                logger.info("save: {}", repository);
                Thread.sleep(10 );
            } catch (Exception e){
                logger.error("", e);
                logger.error("Exception in: {}", repository);
            }
        }
    }

    @Override
    public List<Npm> getNpmRepositoryWithGitUri() {
        return npmRepository.findNoEmptyGitUri();
    }

    @Override
    public List<Npm> getNNpmRepositoryWithGitUri(int amount) {
        return npmRepository.findTenNoEmptyGitUri(new PageRequest(0, amount));
    }

    @Override
    public List<Npm> getNpmRepositoryWithGitUri(List<Npm> repositoryWithGitUri, String pattern) {
        return repositoryWithGitUri.stream().filter((Npm npm) -> {
            Pattern p = Pattern.compile(pattern);
            Matcher matcher = p.matcher(npm.getRepositoryUrl());
            return matcher.find();
        }).collect(Collectors.toList());
    }

}
