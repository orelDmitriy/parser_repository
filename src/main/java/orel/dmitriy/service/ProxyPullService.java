package orel.dmitriy.service;

import orel.dmitriy.model.Proxy;

public interface ProxyPullService {
    int size();
    void addProxy(Proxy proxy);
    void addProxy(String ip, int port);
    Proxy getUnlockProxy();
}
