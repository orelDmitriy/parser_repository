package orel.dmitriy.service;

import orel.dmitriy.model.Git;
import orel.dmitriy.model.Npm;

import java.util.List;

public interface GitHubService {
    List<Git> getAndSaveInfoFromNpmUrls(List<Npm> uris);
}
