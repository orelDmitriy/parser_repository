package orel.dmitriy.service;

import orel.dmitriy.model.ResultDto;

import java.util.List;

public interface SenderService {
    List<ResultDto> sendAll(List<String> urls);
}
