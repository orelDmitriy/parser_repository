package orel.dmitriy.service;

import orel.dmitriy.model.Npm;

import java.util.List;

public interface NpmService {
    void save(List<Npm> repositories);
    List<Npm> getNpmRepositoryWithGitUri();
    List<Npm> getNpmRepositoryWithGitUri(List<Npm> repositoryWithGitUri, String pattern);
    List<Npm> getNNpmRepositoryWithGitUri(int amount);
}
