package orel.dmitriy.service;

import orel.dmitriy.model.Git;
import orel.dmitriy.model.Issue;

import java.util.List;

public interface IssueService {
    List<Issue> getAndSaveInfoFromGitUrls(Git git);
}
