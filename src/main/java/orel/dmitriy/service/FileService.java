package orel.dmitriy.service;

import orel.dmitriy.model.Npm;

import java.util.List;

public interface FileService {
    void createOrClearFile(String path);
    void append(String path, String text);
    List<String> getLines(String path);
    public void writeFile(List<Npm> listDto);
    public void writeFile(String string);
}
