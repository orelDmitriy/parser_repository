package orel.dmitriy.service;

public interface RequestService {
    String getJson(String uriStr);
    String getBigJson(String uri);
    String getJsonFromFile(String filePath);
}
