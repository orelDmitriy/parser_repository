package orel.dmitriy.repository;

import orel.dmitriy.model.Npm;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NpmRepository extends JpaRepository<Npm, Long> {
    @Query("Select c from Npm c where c.repositoryUrl NOT LIKE ''")
    List<Npm> findNoEmptyGitUri();
    @Query("Select c from Npm c where c.repositoryUrl NOT LIKE ''")
    List<Npm> findTenNoEmptyGitUri(Pageable pageRequest);
}
