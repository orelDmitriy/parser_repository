package orel.dmitriy;

public class Settings {
    public static final int MAX_THREADS = 10;
    public static final int TIME_OUT_MINIMUM = 5;
    public static final int TIME_OUT_DELTA = 5;
    public static final String PROXY_FILE_PATH = "D:\\workspace\\springmvcjavaconfig\\src\\main\\resources\\proxy.txt";
    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";
    public static final int SECOND = 1000;
}
