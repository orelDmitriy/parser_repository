package orel.dmitriy.controller;

import orel.dmitriy.model.Npm;
import orel.dmitriy.service.GitHubService;
import orel.dmitriy.service.NpmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class HelloWorldController {

    private static final Logger logger = LoggerFactory.getLogger(HelloWorldController.class);

    @Autowired
    private NpmService npmService;

    @Autowired
    private GitHubService gitHubService;

    @RequestMapping(value = "/")
    public String hello() throws Exception {

        logger.debug("started");
        //List<Npm> repositoryWithGitUri = npmService.getNpmRepositoryWithGitUri();
        List<Npm> repositoryWithGitUri = npmService.getNNpmRepositoryWithGitUri(1000);
        logger.info("size: {}", repositoryWithGitUri.size());
        List<Npm> correctUri = getInfoForUri(repositoryWithGitUri, "github\\.com(/[-_\\d\\w]+){2}", "npm with correct git uri");
        gitHubService.getAndSaveInfoFromNpmUrls(correctUri);

        getInfoForUri(repositoryWithGitUri, "bitbucket\\.org(/[-_\\d\\w]+){2}", "bitbucket");
        logger.debug("completed");
        return "hello";
    }

    public List<Npm> getInfoForUri(List<Npm> repository, String pattern, String msg){
        List<Npm> correctUri = npmService.getNpmRepositoryWithGitUri(repository, pattern);
        logger.info("{}: {}",msg, correctUri.size());
        return correctUri;
    }


}
